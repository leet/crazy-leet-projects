# hdmi-switcher-wifi-controller

## Getting started

## Description
Using an HDMI switcher and a ESP-8266 or ESP32, be able to switch between HDMI inputs.

## Proof of Concept
Using what I have on hand, was an HDMI 3 port in with one port out and a IR remote.

Found a AG71100 at the heart of my device, where I was able to track down technically info https://img.jdzj.com/UserDocument/mallpic/QQ1659747718/dn/zl8303.pdf

## Ideas of usage
Using ESPEasy, with webhooks or mqtt, we can see current HDMI port selected using optocoupler to monitor the LEDs as inputs. Also using an optocoupler as a solid state relay output, we can press the button which would switch ports.

We can change ports in sequence, by triggering the optocoupler/SSR. Did notice there was ```RXSEL```, which different impedance. I think this is used to switch directly to an HDMI port number, possible via the IR remote.

## Notice
Seems there are many different options for number of ports, HDMI level support and bandwidth over the HDMI signal, like 4K or color depth.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
I am not the first to think of this - linked from https://forums.raspberrypi.com/viewtopic.php?t=56302 with video https://www.youtube.com/watch?v=jZvw1KLf1hY

Got the idea to use the ```optocoupler``` from
```mdevaev``` Maxim Devaev of https://pikvm.org / https://github.com/pikvm/pikvm

## License
For open source projects, say how it is licensed.

## Project status
Still in proof of concept.
